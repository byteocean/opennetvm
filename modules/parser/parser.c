#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/queue.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include <rte_common.h>
#include <rte_mbuf.h>
#include <rte_ip.h>

#include "onvm_nflib.h"
#include "onvm_pkt_helper.h"

#define NF_TAG "parser"

/* Struct that contains information about this NF */
struct onvm_nf_info *nf_info;

/* number of package between each print */
static uint32_t print_delay = 1000000;

static uint32_t destination;


/*Print a usage message*/
static void usage(const char *progname)
{
    printf("Usage: %s [EAL args] -- [NF_LIB args] -- -d <destination> \
           -p <print_delay>\n\n",progname);
}

static int parse_app_args(int argc, char *argv[], const char *progname)
{
    int c, dst_flag = 0;
    while((c = getopt(argc, argv, "d:p:")) != -1)
    {
        switch(c)
        {
            case 'd':
                destination = strtoul(optarg, NULL, 10);
                dst_flag = 1;
                break;
            case 'p':
                print_delay = strtoul(optarg, NULL, 10);
                break;
            case '?':
                usage(progname);
                if(optopt == 'd')
                    RTE_LOG(INFO, APP, "Option -%c requires an argument.\n", optopt);
                else if(optopt == 'p')
                    RTE_LOG(INFO, APP, "Option -%c requires an argument.\n", optopt);
                else if(isprint(optopt))
                    RTE_LOG(INFO, APP, "Unknown option `-%c'.\n", optopt);
                else
                    RTE_LOG(INFO, APP, "Unknown option character `\\x%x'.\n", optopt);
                return -1;
            default:
                usage(progname);
                return -1;
        }
    }
    if(!dst_flag)
    {
        RTE_LOG(INFO, APP, "Parser NF requires destination flag -d.\n");
        return -1;
    }
    return optind;
}

/*
 * This function displays stats. It uses ANSI terminal codes to clear
 * screen when called. It is called from a single non-master
 * thread in the server process, when the process is run with more
 * than one lcore enabled.
 */
static void do_stats_display(struct rte_mbuf* pkt)
{
    const char clr[] = {27, '[', '2', 'J', '\0'};
    const char topleft[] = {27, '[', '1', ';', '1', 'H', '\0'};
    static uint64_t pkt_process = 0;
    struct ofp_hdr* ofp;

    pkt_process += print_delay;

    /* Clear screen and move to top left */
    printf("%s%s", ctr, topLeft);

    printf("PACKETS\n");
    printf("-----\n");
    printf("Port : %d\n", pkt->port);
    printf("Size : %d\n", pkt->pkt_len);
    printf("N°   : %"PRIu64"\n", pkt_process);
    printf("\n\n");

    ofp = onvm_pkt_ofp_hdr(pkt);
    if(ofp != NULL)
    {
        onvm_pkt_print_ofp(pkt);
    }
    else
    {
        printf("No OpenFlow 1.3 header found\n");
    }
}

static int packet_handler(struct rte_mbuf* pkt, struct onvm_pkt_meta *meta)
{
    static uint32_t counter = 0;
    if(++counter == print_delay)
    {
        do_stats_display(pkt);
        counter = 0;
    }
    /*
    we need to set the meta->destination below
    */
    meta->action = ONVM_NF_ACTION_TONF;
}

int main(int argc, char *argv[])
{
    int arg_offset;

    const char *progname = argv[0];

    if((arg_offset = onvm_nflib_init(argc, argv, NF_TAG)) < 0)
        return -1;
    argc -= arg_offset;
    argv += arg_offset;

    if(parse_app_args(argc, argv, progname) < 0)
    {
        onvm_nflib_stop();
        rte_exit(EXIT_FAILURE, "Invalid command-line arguments\n");
    }
    onvm_nflib_run(nf_info, &packet_handler);
    printf("If we reach here, program is ending\n");
    return 0;
}
